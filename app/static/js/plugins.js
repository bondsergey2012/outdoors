!function(){function n(n){this.integrations=n}function i(){return"function"==typeof l.currentEntityPath&&l.currentEntityPath()?l.currentEntityPath():window.location.pathname}function t(){return"function"==typeof l.currentEntityTitle&&l.currentEntityTitle()?l.currentEntityTitle():document.title}function e(){return"function"==typeof l.currentEntitySearchParams?"?"+l.currentEntitySearchParams():null}function o(){return"function"==typeof l.currentEntityUrl?l.currentEntityUrl():null}function r(){var n=o();if(n){var i=e();return i?n+i:n}return null}function a(){var n=i();if(n){var t=e();return t?n+t:n}return null}function w(n){if(c&&n.indexOf("trackify")>-1){var i=!!window.fbq,t=["init"],e=["track.PageView"],o=[],r=[];if(window.fbq=function(){var n=arguments[0],i=n+"."+arguments[1];t.indexOf(n)>-1&&o.indexOf(n)>-1||e.indexOf(i)>-1&&r.indexOf(i)>-1||(o.push(n),r.push(i),window.fbq.callMethod?window.fbq.callMethod.apply(window.fbq,arguments):window.fbq.queue.push(arguments))},window._fbq=window._fbq||window.fbq,window.fbq.push=window.fbq,window.fbq.loaded=!0,window.fbq.version="2.0",window.fbq.queue=[],!i){var a=document.createElement("script");a.async=!0,a.src="https://connect.facebook.net/en_US/fbevents.js";var w=document.getElementsByTagName("script")[0];w.parentNode.insertBefore(a,w)}}}function f(){if(!p&&(p=!0,void 0!==window.ga)){var n=r(),i=a(),e=t();n&&window.ga("set","location",n),i&&window.ga("set","page",i),e&&window.ga("set","title",e)}}var c=!1,u=[],d=!1,s="basic",p=!1;n.prototype.init=function(){if(!c){c=!0,w(this.integrations);for(var n=0,i=u.length;n<i;n++)u[n].call();u=[]}},n.prototype.ready=function(n){"function"==typeof n&&(c?n.call():u.push(n))},n.prototype.page=function(){if(!d){d=!0;var n=arguments;void 0!==l&&"function"==typeof l.onEntityStateLoaded?(window.ShopifyAnalytics.lib.url=function(){var n=r();if(n)return n;var i=this.canonical();if(i)return i.indexOf("?")>0?i:i+window.location.search;var t=window.location.href,e=t.indexOf("#");return-1===e?t:t.slice(0,e)},window.ShopifyAnalytics.lib.pageDefaults=function(){var n=window.location.href,o=n.indexOf("?");return n=-1===o?"":n.slice(o),o=n.indexOf("#"),n=-1===o?n:n.slice(0,o),n="?"===n?"":n,{path:i(),referrer:document.referrer,search:e()||n,title:t(),url:this.url(),properties:{}}},window.ZipifyPages.SplitTest.onEntityStateLoaded(function(){window.ShopifyAnalytics.lib.page.call(window.ShopifyAnalytics.lib,n)})):window.ShopifyAnalytics.lib.page.call(window.ShopifyAnalytics.lib,n)}},window.ZipifyPages=window.ZipifyPages||{},window.ZipifyPages.currency="USD",window.ZipifyPages.shopDomain="outdoors-tactical.myshopify.com",window.ZipifyPages.integrations="".split(",");var l=window.ZipifyPages.SplitTest,y=new n(window.ZipifyPages.integrations);void 0!==l&&("basic"===s?(window.GoogleAnalyticsObject="ga",window.ga=window.ga||function(){window.ga.q.push(arguments),"create"===arguments[0]&&f()},window.ga.q=window.ga.q||[],window.ga.l=1*new Date):y.ready(f)),window.ZipifyPages.ShopifyAnalytics=y,window.ZipifyPages.eventsSubscriptions=[],window.ZipifyPages.on=function(n,i){window.ZipifyPages.eventsSubscriptions.push([n,i])}}();


(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K8LQC36');


dataLayer.push({
    'event':'remarketingTriggered2',
    'google_tag_params': {
        ecomm_prodid: '32905836294',
        ecomm_pagetype: 'product',
        ecomm_totalvalue: 32.90,
    }
});


//<![CDATA[
var Shopify = Shopify || {};
Shopify.shop = "outdoors-tactical.myshopify.com";
Shopify.theme = {"name":"OT Theme 2.1","id":238059526,"theme_store_id":null,"role":"main"};
Shopify.theme.handle = "null";
Shopify.theme.style = {"id":null,"handle":null};

//]]>


//<![CDATA[
(function() {
    function asyncLoad() {
        var urls = ["https:\/\/media.conversio.com\/scripts\/shopify.js?shop=outdoors-tactical.myshopify.com","https:\/\/app.redretarget.com\/sapp\/ptag.php?shop=outdoors-tactical.myshopify.com\u0026shop=outdoors-tactical.myshopify.com","\/\/cdn.ywxi.net\/js\/1.js?shop=outdoors-tactical.myshopify.com","\/\/d22qiiscf4hrnt.cloudfront.net\/st_10831940_415_1484259089.js?shop=outdoors-tactical.myshopify.com","\/\/widget.wickedreports.com\/SaliusMarkingInc\/trackfu.js?shop=outdoors-tactical.myshopify.com","\/\/livesearch.okasconcepts.com\/js\/livesearch.init.min.js?shop=outdoors-tactical.myshopify.com","\/\/shopify.privy.com\/widget.js?shop=outdoors-tactical.myshopify.com","https:\/\/loox.io\/widget\/VJsVufACz\/loox.js?shop=outdoors-tactical.myshopify.com","\/\/cdn.ywxi.net\/js\/partner-shopify.js?shop=outdoors-tactical.myshopify.com","https:\/\/api.carts.guru\/d372fe24-795b-4c9f-afeb-3bd48e884f55\/shopify\/scripttag?v=1487073756000\u0026shop=outdoors-tactical.myshopify.com","https:\/\/static.shopmsg.me\/js\/420582314815649\/shopmessage.js?shop=outdoors-tactical.myshopify.com","https:\/\/chimpstatic.com\/mcjs-connected\/js\/users\/490420e3b6c1d0d1b16ca2d44\/ba67ffd601df10f16df6d0c75.js?shop=outdoors-tactical.myshopify.com","https:\/\/yopify.com\/api\/yo\/js\/yo\/361a3ee950d7fe16cce2526e97dc3ee2\/bootstrap.js?v=201710101507649121\u0026shop=outdoors-tactical.myshopify.com","https:\/\/sdk.beeketing.com\/js\/beeketing.js?shop=outdoors-tactical.myshopify.com"];
        for (var i = 0; i < urls.length; i++) {
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = urls[i];
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        }
    };
    if(window.attachEvent) {
        window.attachEvent('onload', asyncLoad);
    } else {
        window.addEventListener('load', asyncLoad, false);
    }
})();

//]]>


//<![CDATA[
var __st={"a":10831940,"offset":-18000,"reqid":"6f868200-5dfa-4610-b498-ac86a3744732","pageurl":"outdoorstactical.com\/pages\/mono","s":"pages-240332102","t":"prospect","u":"eb525ba910d3","p":"page","rtyp":"page","rid":240332102};
//]]>


//<![CDATA[
window['GoogleAnalyticsObject'] = 'ga';
window['ga'] = window['ga'] || function() {
    (window['ga'].q = window['ga'].q || []).push(arguments);
};
window['ga'].l = 1 * new Date();

//]]>



var _gaUTrackerOptions = {'allowLinker': true};ga('create', 'UA-74333841-1', 'auto', _gaUTrackerOptions);try{document.getElementsByClassName("main__footer")[0].style.display = 'none';

    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-K8LQC36');}catch(e){};ga('send', 'pageview');
(function(){
    ga('require', 'linker');
    function addListener(element, type, callback) {
        if (element.addEventListener) {
            element.addEventListener(type, callback);
        }
        else if (element.attachEvent) {
            element.attachEvent('on' + type, callback);
        }
    }
    function decorate(event) {
        event = event || window.event;
        var target = event.target || event.srcElement;
        if (target && (target.action || target.href)) {
            ga(function (tracker) {
                var linkerParam = tracker.get('linkerParam');
                document.cookie = '_shopify_ga=' + linkerParam + '; ' + 'path=/';
            });
        }
    }
    addListener(window, 'load', function(){
        for (var i=0; i<document.forms.length; i++) {
            if(document.forms[i].action && document.forms[i].action.indexOf('/cart') >= 0) {
                addListener(document.forms[i], 'submit', decorate);
            }
        }
        for (var i=0; i<document.links.length; i++) {
            if(document.links[i].href && document.links[i].href.indexOf('/checkout') >= 0) {
                addListener(document.links[i], 'click', decorate);
            }
        }
    })
}());



window.ShopifyAnalytics = window.ShopifyAnalytics || {};
window.ShopifyAnalytics.meta = window.ShopifyAnalytics.meta || {};
window.ShopifyAnalytics.meta.currency = 'USD';
var meta = {"page":{"pageType":"page","resourceType":"page","resourceId":240332102}};
for (var attr in meta) {
    window.ShopifyAnalytics.meta[attr] = meta[attr];
}


window.ShopifyAnalytics.merchantGoogleAnalytics = function() {

};




(function () {
    var customDocumentWrite = function(content) {
        var jquery = null;

        if (window.jQuery) {
            jquery = window.jQuery;
        } else if (window.Checkout && window.Checkout.$) {
            jquery = window.Checkout.$;
        }

        if (jquery) {
            jquery('body').append(content);
        }
    };

    var trekkie = window.ShopifyAnalytics.lib = window.trekkie = window.trekkie || [];
    if (trekkie.integrations) {
        return;
    }
    trekkie.methods = [
        'identify',
        'page',
        'ready',
        'track',
        'trackForm',
        'trackLink'
    ];
    trekkie.factory = function(method) {
        return function() {
            var args = Array.prototype.slice.call(arguments);
            args.unshift(method);
            trekkie.push(args);
            return trekkie;
        };
    };
    for (var i = 0; i < trekkie.methods.length; i++) {
        var key = trekkie.methods[i];
        trekkie[key] = trekkie.factory(key);
    }
    trekkie.load = function(config) {
        trekkie.config = config;
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.onerror = function(e) {
            (new Image()).src = '//v.shopify.com/internal_errors/track?error=trekkie_load';
        };
        script.async = true;
        script.src = 'https://cdn.shopify.com/s/javascripts/tricorder/trekkie.storefront.min.js?v=2017.09.05.1';
        var first = document.getElementsByTagName('script')[0];
        first.parentNode.insertBefore(script, first);
    };
    trekkie.load(
        {"Trekkie":{"appName":"storefront","development":false,"defaultAttributes":{"shopId":10831940,"isMerchantRequest":null,"themeId":238059526,"themeCityHash":14063011531703031080}},"Performance":{"navigationTimingApiMeasurementsEnabled":true,"navigationTimingApiMeasurementsSampleRate":0.1},"Facebook Pixel":{"pixelIds":["791925147600148"],"agent":"plshopify1.2"},"Session Attribution":{}}
    );

    var loaded = false;
    trekkie.ready(function() {
        if (loaded) return;
        loaded = true;

        window.ShopifyAnalytics.lib = window.trekkie;


        var originalDocumentWrite = document.write;
        document.write = customDocumentWrite;
        try { window.ZipifyPages.ShopifyAnalytics.init();window.ShopifyAnalytics.merchantGoogleAnalytics.call(this); } catch(error) {};
        document.write = originalDocumentWrite;


        window.ShopifyAnalytics.lib.page(
            null,
            {"pageType":"page","resourceType":"page","resourceId":240332102}
        );


    });


    var eventsListenerScript = document.createElement('script');
    eventsListenerScript.async = true;
    eventsListenerScript.src = "//cdn.shopify.com/s/assets/shop_events_listener-f2c5800305098f0ebebdfa7d980c9abf56514c46d5305e97a7c476f7c9116163.js";
    document.getElementsByTagName('head')[0].appendChild(eventsListenerScript);

})();


//<![CDATA[
window.Shopify = window.Shopify || {};
window.Shopify.Checkout = window.Shopify.Checkout || {};
window.Shopify.Checkout.apiHost = "outdoors-tactical.myshopify.com";
window.Shopify.Checkout.rememberMeHost = "pay.shopify.com";
window.Shopify.Checkout.rememberMeAccessToken = "MzdaSVRtYnRXNDJLRjlaSitiVkNqbVBVVC9SL2ZBNVBXTHM4T05uNzNsN0NIdFRrVDAyOGZRbXBWTmhNdHdReC0tOWtUZjZ0MmdZSHhGajlBQWtYWHN6QT09--0448b54654e72abf59833f4d46705784abd502aa";
window.Shopify.Checkout.sheetStyleSheetUrl = "\/\/cdn.shopify.com\/s\/assets\/shared\/sheet\/main-89599591dd137add8a9ec475b2a321ec5708359948584a168790c7bdf770721a.css";

//]]>


//<![CDATA[
window.ShopifyPaypalV4VisibilityTracking = true;
//]]>


