var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var htmlmin = require('gulp-htmlmin');
var uglify = require('gulp-uglify');
var pump = require('pump');
var purify = require('gulp-purifycss');

gulp.task('css', function () {
    gulp.src('app/**/*.css')
        .pipe(purify(['app/**/*.html', 'app/**/*.js']))
        .pipe(cssmin())
        .pipe(gulp.dest('dist'));
});


gulp.task('html', function() {
    return gulp.src('app/**/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('dist'));
});



gulp.task('js', function (cb) {
    pump([
            gulp.src('app/**/*.js'),
            uglify(),
            gulp.dest('dist')
        ],
        cb
    );
});

gulp.task('default',['css', 'html', 'js'])